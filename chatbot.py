import random
import json
import pickle
import numpy as np

import nltk
from nltk.stem import WordNetLemmatizer

from tensorflow.keras.models import load_model

import speech_recognition as sr
import pyttsx3

#----------------------------------------------------------------------------------#

lemmatizer = WordNetLemmatizer()
intents = json.loads(open('intents.json').read())

words = pickle.load(open('words.pkl', 'rb'))
classes = pickle.load(open('classes.pkl', 'rb'))
model = load_model('chatbot_with_intents.h5')

# Helper used to tokenize and lemmatize words in a sentence
def clean_up_sentence(sentence):
  sentence_words = nltk.word_tokenize(sentence)
  sentence_words = [lemmatizer.lemmatize(word) for word in sentence_words]
  return sentence_words

# Helper used to create a bag of words from a sentence
def bag_of_words(sentence):
  sentence_words = clean_up_sentence(sentence)
  bag = [0] * len(words)
  for word in sentence_words:
    for i, w in enumerate(words):
      if w == word:
        bag[i] = 1
  return np.array(bag)

# Create bag of words, make prediction with model, and return intent tags with probabilities
def predict_class(sentence):
  bag = bag_of_words(sentence)
  prediction = model.predict(np.array([bag]))[0]
  ERROR_THRESHOLD = 0.25
  results = [[i, r] for i,r in enumerate(prediction) if r > ERROR_THRESHOLD]

  # Sorted with highest probability first
  results.sort(key=lambda x: x[1], reverse=True)
  return_list = []
  for r in results:
    return_list.append({'intent': classes[r[0]], 'probability': str(r[1])})
  return return_list

# Handle responses from the console
def get_response(intents_list, intents_json):
  tag = intents_list[0]['intent']
  list_of_intents = intents_json['intents']
  for i in list_of_intents:
    if i['tag'] == tag:
      result = random.choice(i['responses'])
      break
  return result

# Run the chatbot
while True:
  # Recognize utterances through microphone for a single response
  respond = input("\nEnter anything to detect some speech: ")
  message = None
  r = sr.Recognizer()
  try:
    # Initialize, configure, and use microphone
    print("\tListening..")
    with sr.Microphone() as mic:                    # Init
      r.adjust_for_ambient_noise(mic, duration=1) # Config
      audio = r.listen(mic)                         # Use

      # Extract text
      message = r.recognize_google(audio)
      print(f"\tI heard you saying: '{message.lower()}'")

  except:
    print("\nWIZARD SAYS--> I did not recognize that, try again!")
    r = sr.Recognizer()
    continue

  ints = predict_class(message)
  res = get_response(ints, intents)
  print("\tWIZARD SAYS -->", res)