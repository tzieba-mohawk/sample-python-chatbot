import random
import json
import pickle
import numpy as np

import nltk
# Downloads from NLTK needed only first time (v3.8 shell)
# nltk.download('punkt')
# nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout
from tensorflow.keras.optimizers import SGD

lemmatizer = WordNetLemmatizer()

intents = json.loads(open('intents.json').read())

words = []
classes = []
documents = []
ignore_chars = ['?', '!', ',', '.']

# Tokenize patterns
for intent in intents['intents']:
  for pattern in intent['patterns']:
    word_list = nltk.word_tokenize(pattern)
    words.extend(word_list)
    documents.append((word_list, intent['tag']))

    if intent['tag'] not in classes:
      classes.append(intent['tag'])

# Lemmatize words and sort the distinct set of words from intents
words = [lemmatizer.lemmatize(word) for word in words if word not in ignore_chars]
words = sorted(set(words))

# Sort distinct set of classes
classes = sorted(set(classes))

# Save words and classes to a pickle file
pickle.dump(words, open('words.pkl', 'wb'))
pickle.dump(classes, open('classes.pkl', 'wb'))

training = []
output_empty = [0] * len(classes)

# Create bag of words from word patterns in all documents
for document in documents:
  bag = []
  word_patterns = document[0]
  word_patterns = [lemmatizer.lemmatize(word.lower()) for word in word_patterns]
  for word in words:
    bag.append(1) if word in word_patterns else bag.append(0)

  # Copy correctly sized empty output as initial output row
  output_row = list(output_empty)

  # Classify outputs based on intent tags for each document (word_list, tag)
  output_row[classes.index(document[1])] = 1

  # Add each word bag and its classification to training data
  training.append([bag, output_row])

# Randomize training data
random.shuffle(training)
training = np.array(training)

train_x = list(training[:,0])
train_y = list(training[:,1])

model = Sequential()
model.add(Dense(128, input_shape=(len(train_x[0]),), activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(train_y[0]), activation='softmax'))

sgd = SGD(learning_rate=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])

# Save model as hist  ot be saved into a .h5 file
hist = model.fit(np.array(train_x), np.array(train_y), epochs=200, batch_size=5, verbose=1)

model.save('chatbot_with_intents.h5', hist)
print("Done")