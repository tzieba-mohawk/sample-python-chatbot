README

- Install python 3.9.7 or greater.

- Speech Recognition Pre-requisites:
	Open command prompt as an Administrator and enter these commands:
		1. pip3 install speechrecognition	(main module)
		2. pip3 install pyttsx3			(has pyaudio dependency)
		3. pip install pipwin			(needed to install pyaudio)
		4. pipwin install pyaudio
- Chatbot Pre-requisites:
	Open command prompt as an Administrator and enter these commands:
		1. pip install nltk
		2. pip install tensorflow

- Running Instructions:
	1. Ensure that a microphone is connected and enabled.
	2. Open the command prompt in the directory where this repository is stored.
	3. Uncomment lines 8 and 9 in training.py to initially cache required downloads for nltk (can be re-commented after being cached).
	4. Enter command 'python training.py'
	5. Enter command 'exit()' to exit python.
	6. Enter command 'python chatbot.py' to start the chatbot.
	7. Follow command line instructions to interact with the chatbot (e.g. say "Hello how are you?").